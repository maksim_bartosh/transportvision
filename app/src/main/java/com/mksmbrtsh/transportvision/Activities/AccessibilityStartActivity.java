package com.mksmbrtsh.transportvision.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.mksmbrtsh.transportvision.Loaders.LoaderVehicle;
import com.mksmbrtsh.transportvision.Models.Route;
import com.mksmbrtsh.transportvision.Models.Station;
import com.mksmbrtsh.transportvision.Models.Transport;
import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.Models.Vehicle;
import com.mksmbrtsh.transportvision.R;
import com.mksmbrtsh.transportvision.utils.TimeString;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import static com.mksmbrtsh.transportvision.Const.TIMEOUT_REFRESH;
import static com.mksmbrtsh.transportvision.Const.TIMEOUT_SPEAK;
import static com.mksmbrtsh.transportvision.Const.TIME_TO_ARRIVE;

public class AccessibilityStartActivity extends AccessibilityBaseActivity implements LoaderManager.LoaderCallbacks<LoaderVehicle.RefreshListVehicles>,  View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private boolean sound = true;
    private TextView mCurrent;
    private ImageView mDin;
    private ArrayList<Vehicle> mDeprecatedStations;
    private ArrayList<Vehicle> mAllStations;
    private Vehicle mCurrentStation;
    private Boolean mLaunch;
    private Handler mCountDownTimer;
    private Runnable r = new Runnable() {
        @Override
        public void run() {
            AccessibilityStartActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG,"mCountDownTimer: restartLoader!");
                    getSupportLoaderManager().restartLoader(0, null, AccessibilityStartActivity.this);
                }
            });

        }
    };

    private Route mRoute;
    private TypeTransport mTypeTransport;
    private Station mStationFrom;
    private Station mStationTo;
    private Transport mTransport;

    private long mTimeStartNewSpeak;

    private enum STATE {
        NOTHING,
        ARRIVES,
        NOT_FOUND_VEHICLE
    }

    private STATE mCurrentState = STATE.NOTHING;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mRoute = getIntent().getParcelableExtra("route");
        mTypeTransport = getIntent().getParcelableExtra("type_transport");
        mStationFrom = getIntent().getParcelableExtra("station_from");
        mStationTo = getIntent().getParcelableExtra("station_to");
        mTransport = getIntent().getParcelableExtra("vehicle");

        TextView title = findViewById(R.id.text_choice_map_point2);
        title.setText(title.getText() + " " + mTypeTransport.name +" " +mRoute.name + " " +getString(R.string.from) + " " + mStationFrom.station_name + " " +getString(R.string.to) + " " + mStationTo.station_name);

        findViewById(R.id.back2).setOnClickListener(this);
        mDin = findViewById(R.id.din);
        mDin.setOnClickListener(this);
        mCurrent = findViewById(R.id.current);
        mCurrent.setOnClickListener(this);
        if(savedInstanceState != null){
            mCurrent.setText(savedInstanceState.getString("current"));
            mDeprecatedStations = savedInstanceState.getParcelableArrayList("deprecated_stations");
            mCurrentStation = savedInstanceState.getParcelable("current_station");
            mTimeStartNewSpeak = savedInstanceState.getLong("time_to_refresh");
            mLaunch = savedInstanceState.getByte("launch") == (byte)1;
            mAllStations = savedInstanceState.getParcelableArrayList("all_stations");
        } else {
            mDeprecatedStations = new ArrayList<Vehicle>();
            mAllStations = new ArrayList<Vehicle>();
            mCurrentStation = null;
            mTimeStartNewSpeak = 0;
            mLaunch = true;
        }
        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    public void onLoadData() {
        getSupportLoaderManager().initLoader(0, null, AccessibilityStartActivity.this);
    }

    @Override
    public void onSpeakComplete() {
        switch (mCurrentState){
            case ARRIVES:
                Log.d(TAG,"onSpeakComplete: ARRIVES!");
                startActivitySelectTypeTransport();
                break;
            case NOT_FOUND_VEHICLE:
                Log.d(TAG,"onSpeakComplete: NOT FOUND VEHICLE!");
                startActivitySelectTypeTransport();
                break;
            case NOTHING: break;
        }
        Log.d(TAG,"onSpeakComplete: NOTHING!");
        mCurrentState = STATE.NOTHING;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("current", mCurrent.getText().toString());
        outState.putParcelableArrayList("deprecated_stations", mDeprecatedStations);
        outState.putParcelable("current_station", mCurrentStation);
        outState.putLong("time_to_refresh", mTimeStartNewSpeak);
        outState.putByte("launch", mLaunch ? (byte)1:(byte)0);
        outState.putParcelableArrayList("all_stations", mAllStations);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AccessibilitySelectTransportActivity.class);
        intent.putExtra("vehicle", mTransport);
        intent.putExtra("station_from",mStationFrom);
        intent.putExtra("route", mRoute);
        intent.putExtra("type_transport", mTypeTransport);
        intent.putExtra("station_to", mStationTo);
        this.startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.back2){
            onBackPressed();
        } else if(view.getId() == R.id.current) {
            if(mCurrent.getText().toString().equals(getString(R.string.finish))){
                startActivitySelectTypeTransport();
            } else if(mCurrent.getText().toString().equals(getString(R.string.error_load))){
                getSupportLoaderManager().restartLoader(0, null, AccessibilityStartActivity.this);
            }
        } else {
            sound = !sound;
            if(sound) {
                mDin.setImageResource(R.drawable.ic_icn_sound_on);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.sound);
                    mDin.setContentDescription(voice);
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                }
            }
            else {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.nosound);
                    mDin.setContentDescription(voice);
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                }
                mDin.setImageResource(R.drawable.ic_icn_sound_off);
            }
        }
    }

    @NonNull
    @Override
    public Loader<LoaderVehicle.RefreshListVehicles> onCreateLoader(int id, @Nullable Bundle args) {
        if (mCurrent.getText().toString().equals(R.string.plz_wait6) && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
            String utteranceId = UUID.randomUUID().toString();
            String voice = getString(R.string.plz_wait6);
            textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }
        if(mTimeStartNewSpeak == 0)
            mTimeStartNewSpeak = new Date().getTime() + TIMEOUT_SPEAK;
        return new LoaderVehicle(this, mTransport.obj_id, mDeprecatedStations, mCurrentStation, mStationFrom, mAllStations);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<LoaderVehicle.RefreshListVehicles> loader, LoaderVehicle.RefreshListVehicles data) {
        long time = new Date().getTime();
        switch (data.status) {
            case JSON_ERROR:
                errorFatalCallback();
                break;
            case IO_ERROR:
                errorCallback();
                break;
            case NEW_CURRENT_STATION:
                mTimeStartNewSpeak = time + TIMEOUT_SPEAK;
                setNewCurrentStation(data);
                break;
            case OLD_CURRENT:
                if(time > mTimeStartNewSpeak) {
                    mTimeStartNewSpeak = time + TIMEOUT_SPEAK;
                    refreshCurrentWithSpeakFromTimeout(data);
                } else {
                    refreshCurrentWithoutSpeak(data);
                }
                break;
            case REFRESH_CURRENT:
                mTimeStartNewSpeak = time + TIMEOUT_SPEAK;
                if(mLaunch) {
                    mLaunch = false;
                    refreshCurrentWithoutSpeak(data);
                } else
                    refreshCurrentWithSpeak(data);
                break;
            case ARRAY_JSON_EMPTY:
                if(mAllStations.size() > 1){
                    if(mAllStations.get(1).stid.equals(String.valueOf(mStationTo.station_id))) {
                        arrived();
                    }
                }
                break;
            case ALL_STATION_DEPRECATED:
                deprecatedRoute();
                break;
        }
    }


    private void refreshCurrentWithSpeak(LoaderVehicle.RefreshListVehicles data) {
        mCurrentStation = data.current;
        mDeprecatedStations = data.deprecatedVehicles;
        String s = getCurrentString(data);
        mCurrent.setText(s);
        mCountDownTimer = new Handler();
        mCountDownTimer.postDelayed(r, TIMEOUT_REFRESH);
        if (sound && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCurrentState = STATE.NOTHING;
            String utteranceId = UUID.randomUUID().toString();
            String voice = mCurrent.getText().toString();
            textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }
        if (data.current.stid.equals(String.valueOf(mStationTo.station_id)) && data.current.getArrt() < TIME_TO_ARRIVE) {
            arrived();
            return;
        }
        boolean ok = false;
        for(Vehicle v : data.allVehicles) {
            if(v.stid.equals(String.valueOf(mStationTo.station_id))) {
                ok = true;
                if(v.getArrt() < TIME_TO_ARRIVE) {
                    arrived();
                    return;
                }
                break;
            }
        }
        if(!ok) {
            arrived();
            return;
        }
    }

    private void refreshCurrentWithSpeakFromTimeout(LoaderVehicle.RefreshListVehicles data) {
        mCountDownTimer = new Handler();
        mCountDownTimer.postDelayed(r, TIMEOUT_REFRESH);
        if (sound && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCurrentState = STATE.NOTHING;
            String utteranceId = UUID.randomUUID().toString();
            String voice = mCurrent.getText().toString();
            textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }
    }

    private void refreshCurrentWithoutSpeak(LoaderVehicle.RefreshListVehicles data) {
        mCountDownTimer = new Handler();
        mCountDownTimer.postDelayed(r, TIMEOUT_REFRESH);
        if (data.current.stid.equals(String.valueOf(mStationTo.station_id)) && data.current.getArrt() < TIME_TO_ARRIVE) {
            arrived();
            return;
        }
        boolean ok = false;
        for(Vehicle v : data.allVehicles) {
            if(v.stid.equals(String.valueOf(mStationTo.station_id))) {
                ok = true;
                if(v.getArrt() < TIME_TO_ARRIVE) {
                    arrived();
                    return;
                }
                break;
            }
        }
        if(!ok) {
            arrived();
            return;
        }
    }

    private void deprecatedRoute() {
        mCurrent.setText(R.string.not_found_inf_vehicle);
        if (mCountDownTimer != null) {
            mCountDownTimer.removeCallbacksAndMessages(null);
        }
        getSupportLoaderManager().destroyLoader(0);
        if (sound && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCurrentState = STATE.ARRIVES;
            String utteranceId = UUID.randomUUID().toString();
            String voice = mCurrent.getText().toString();
            textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        } else {
            startActivitySelectTypeTransport();
        }
    }

    private void arrived(){
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.finish));
        if (mCountDownTimer != null) {
            mCountDownTimer.removeCallbacksAndMessages(null);
        }
        mCurrent.setText(sb.toString());
        getSupportLoaderManager().destroyLoader(0);
        if (sound&& android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCurrentState = STATE.ARRIVES;
            String utteranceId = UUID.randomUUID().toString();
            String voice = mCurrent.getText().toString();
            textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
        } else {
            startActivitySelectTypeTransport();
        }
    }

    private void setNewCurrentStation(LoaderVehicle.RefreshListVehicles data) {
        mCurrentStation = data.current;
        mDeprecatedStations = data.deprecatedVehicles;
        StringBuilder sb = new StringBuilder();
        sb.append(getCurrentString(data));
        mCurrent.setText(sb.toString());
        if (sound && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCurrentState = STATE.NOTHING;
            String utteranceId = UUID.randomUUID().toString();
            String voice = mCurrent.getText().toString();
            textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }
        mCountDownTimer = new Handler();
        mCountDownTimer.postDelayed(r, TIMEOUT_REFRESH);
        if (data.current.stid.equals(String.valueOf(mStationTo.station_id)) && data.current.getArrt() < TIME_TO_ARRIVE) {
            arrived();
            return;
        }
    }

    private void errorCallback() {
        mCurrentState = STATE.NOTHING;
        if(mCurrentStation == null) {
            mCurrent.setText(R.string.error_load);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String utteranceId = UUID.randomUUID().toString();
                textToSpeech.speak(mCurrent.getText().toString(), TextToSpeech.QUEUE_ADD, null, utteranceId);
            }
        } else {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String utteranceId = UUID.randomUUID().toString();
                textToSpeech.speak(getString(R.string.error_load), TextToSpeech.QUEUE_ADD, null, utteranceId);
            } else
                mCurrent.setText(R.string.error_load);
        }
        mCountDownTimer = new Handler();
        mCountDownTimer.postDelayed(r, TIMEOUT_REFRESH);
    }

    private void errorFatalCallback() {
        deprecatedRoute();
    }

    private String getCurrentString(LoaderVehicle.RefreshListVehicles data) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.arrives));
        sb.append(' ');
        sb.append(TimeString.SecondToTimeStr(this, data.current.getArrt()));
        sb.append(' ');

        if(data.current.stid.equals(String.valueOf(mStationFrom.station_id))) {
            sb.append(getString(R.string.on));
            sb.append(' ');
            sb.append(getString(R.string.your_point));
            sb.append(' ');
        } else if(data.current.stid.equals(String.valueOf(mStationTo.station_id))) {
            sb.append(getString(R.string.in));
            sb.append(' ');
            sb.append(getString(R.string.your_end_point));
            sb.append(' ');
        } else {
            sb.append(getString(R.string.on));
            sb.append(' ');
        }
        sb.append(data.current.stname);
        if(data.allVehicles !=null && data.allVehicles.size() > 1 && data.allVehicles.get(1).stid.equals(String.valueOf(mStationTo.station_id))){
            sb.append(' ');
            sb.append(getString(R.string.nextYour));
        }
        String s = sb.toString();
        return s;
    }


    private void startActivitySelectTypeTransport() {
        Intent intent = new Intent(this, SelectTypeTransportActivity.class);
        this.startActivity(intent);
        finish();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<LoaderVehicle.RefreshListVehicles> loader) {

    }

    @Override
    public void onDestroy() {
        if(mCountDownTimer != null){
            mCountDownTimer.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }
}