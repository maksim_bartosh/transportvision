package com.mksmbrtsh.transportvision.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.mksmbrtsh.transportvision.Loaders.LoaderTypesTransport;
import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.R;
import com.mksmbrtsh.transportvision.UI.AdapterChoiceTypeTransport;
import com.mksmbrtsh.transportvision.UI.RecyclerItemClickListener;
import java.util.UUID;

import androidx.loader.app.LoaderManager;

public class SelectTypeTransportActivity extends AccessibilityBaseActivity implements LoaderManager.LoaderCallbacks<TypeTransport[]>, View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private Button mPlzWait;
    private RecyclerView mRecyclerView;

    private int mPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_type_transport);
        mRecyclerView = findViewById(R.id.list_type_transport);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mPlzWait = findViewById(R.id.plz_wait);
        mPlzWait.setOnClickListener(this);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mPosition = position;
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.choice) + " " + ((AdapterChoiceTypeTransport) mRecyclerView.getAdapter()).getItems()[position].name;
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                } else {
                    startActivitySelectRoute(position);
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onLoadData() {
        getSupportLoaderManager().initLoader(0,null, this);
    }

    @Override
    public void onSpeakComplete() {
        if(mPosition != NOT_SELECTED)
            startActivitySelectRoute(mPosition);
    }

    private void startActivitySelectRoute(int positionTypeTransport){
        Intent intent = new Intent(SelectTypeTransportActivity.this, AccessibilitySelectRouteActivity.class);
        intent.putExtra("type_transport", ((AdapterChoiceTypeTransport) mRecyclerView.getAdapter()).getItems()[positionTypeTransport]);
        Log.d(TAG, "startActivitySelectRoute: type_transport='" + ((AdapterChoiceTypeTransport) mRecyclerView.getAdapter()).getItems()[positionTypeTransport].toString() +"'");
        this.startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    @Override
    public void onDestroy() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }
    @NonNull
    @Override
    public Loader<TypeTransport[]> onCreateLoader(int id, @Nullable Bundle args) {
        mPlzWait.setText(R.string.plz_wait1);
        mPlzWait.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setAllImportantForAccessibility(false);
            String utteranceId = UUID.randomUUID().toString();
            String voice = getString(R.string.plz_wait1);
            textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
        }
        return new LoaderTypesTransport(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<TypeTransport[]> loader, TypeTransport[] data) {
        if(data != null) {
            if(data.length == 0) {
                mPlzWait.setText(R.string.empty_type_transport_list);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = mPlzWait.getText().toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
            } else {
                AdapterChoiceTypeTransport actt = new AdapterChoiceTypeTransport(data);
                mRecyclerView.setAdapter(actt);
                mRecyclerView.setVisibility(View.VISIBLE);
                mPlzWait.setVisibility(View.INVISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.loaded_types_transport);
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
            }
        } else {
            mRecyclerView.setVisibility(View.INVISIBLE);
            mPlzWait.setVisibility(View.VISIBLE);
            mPlzWait.setText(R.string.error_load);
            String utteranceId = UUID.randomUUID().toString();
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textToSpeech.speak(mPlzWait.getText().toString(), TextToSpeech.QUEUE_ADD, null, utteranceId);
            }
        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<TypeTransport[]> loader) {

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.plz_wait) {
            if(getSupportLoaderManager().hasRunningLoaders()){
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String voice = getString(R.string.plz_wait1);
                    String utteranceId = UUID.randomUUID().toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                }
            } else
                getSupportLoaderManager().restartLoader(0, null, this);
        } else
            finish();
    }

}
