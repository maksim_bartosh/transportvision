package com.mksmbrtsh.transportvision.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mksmbrtsh.transportvision.Loaders.LoaderTransports;
import com.mksmbrtsh.transportvision.Models.Route;
import com.mksmbrtsh.transportvision.Models.Station;
import com.mksmbrtsh.transportvision.Models.Transport;
import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.R;
import com.mksmbrtsh.transportvision.UI.AdapterChoiceStation;
import com.mksmbrtsh.transportvision.UI.AdapterChoiceTransport;
import com.mksmbrtsh.transportvision.UI.RecyclerItemClickListener;

import java.util.UUID;

import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED;
import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_FOCUSED;
import static com.mksmbrtsh.transportvision.Const.TIMEOUT_REFRESH;
import static com.mksmbrtsh.transportvision.Const.TIMEOUT_SPEAK;

public class AccessibilitySelectTransportActivity extends AccessibilityBaseActivity implements LoaderManager.LoaderCallbacks<Transport[]>, View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private Button mPlzWait;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    private Route mRoute;
    private TypeTransport mTypeTransport;
    private Station mStationFrom;
    private Station mStationTo;

    private Handler mCountDownTimer;

    private String mSelectedId;
    private int mPosition = -1;

    private Runnable r = new Runnable() {
        @Override
        public void run() {
            AccessibilitySelectTransportActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getSupportLoaderManager().restartLoader(0, null, AccessibilitySelectTransportActivity.this);
                }
            });

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_transport);
        mRoute = getIntent().getParcelableExtra("route");
        mTypeTransport = getIntent().getParcelableExtra("type_transport");
        mStationFrom = getIntent().getParcelableExtra("station_from");
        mStationTo = getIntent().getParcelableExtra("station_to");

        TextView title = findViewById(R.id.text_transport);
        title.setText(title.getText() + ": " + mTypeTransport.name +" " + mRoute.name + " " +getString(R.string.from) + " " + mStationFrom.station_name + " " +getString(R.string.to) + " " + mStationTo.station_name);

        mRecyclerView = findViewById(R.id.list_transport);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPlzWait = findViewById(R.id.plz_wait);
        mPlzWait.setOnClickListener(this);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(mCountDownTimer != null){
                        mCountDownTimer.removeCallbacksAndMessages(null);
                    }
                    getSupportLoaderManager().destroyLoader(0);
                    mPosition = position;
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.choice) + " " + ((AdapterChoiceTransport) mRecyclerView.getAdapter()).getArrive(position);
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                } else {
                    startActivityStart(mPosition);
                }
            }


            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        RecyclerViewAccessibilityDelegate rr = new RecyclerViewAccessibilityDelegate(mRecyclerView);
        mRecyclerView.setAccessibilityDelegate(new RecyclerView.AccessibilityDelegate() {
            @Override
            public boolean onRequestSendAccessibilityEvent(ViewGroup host, View child, AccessibilityEvent event) {
                int position;
                switch (event.getEventType()) {
                    case TYPE_VIEW_ACCESSIBILITY_FOCUSED:
                        position = (Integer) child.getTag();
                        mSelectedId = ((AdapterChoiceTransport) mRecyclerView.getAdapter()).getItems()[position].gos_num;
                        break;
                }

                return super.onRequestSendAccessibilityEvent(host, child, event);
            }
        });
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onLoadData() {
        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onSpeakComplete() {
        if(mPosition != NOT_SELECTED)
            startActivityStart(mPosition);
    }

    private void startActivityStart(int positionTransport){
        Intent intent = new Intent(this, AccessibilityStartActivity.class);
        intent.putExtra("vehicle", ((AdapterChoiceTransport) mRecyclerView.getAdapter()).getItems()[positionTransport]);
        intent.putExtra("station_from",mStationFrom);
        intent.putExtra("route", mRoute);
        intent.putExtra("type_transport", mTypeTransport);
        intent.putExtra("station_to", mStationTo);
        Log.d(TAG, "startActivityStart: type_transport='" + mTypeTransport.toString() + "' route='" + mRoute.toString() +"' station_from='" + mStationFrom.toString() +"' station_to='" + mStationTo.toString() + "' vehicle='" + ((AdapterChoiceTransport) mRecyclerView.getAdapter()).getItems()[positionTransport].toString() + "'");
        this.startActivity(intent);
        finish();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AccessibilitySelectStationToActivity.class);
        intent.putExtra("station_from",mStationFrom);
        intent.putExtra("route", mRoute);
        intent.putExtra("type_transport", mTypeTransport);
        intent.putExtra("station_to",mStationTo);
        this.startActivity(intent);
        finish();
    }

    @Override
    public void onDestroy() {
        if(mCountDownTimer != null){
            mCountDownTimer.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }
    @NonNull
    @Override
    public Loader<Transport[]> onCreateLoader(int id, @Nullable Bundle args) {
        if(mRecyclerView.getAdapter() == null) {
            mPlzWait.setText(R.string.plz_wait5);
            mPlzWait.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.INVISIBLE);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setAllImportantForAccessibility(false);
                String utteranceId = UUID.randomUUID().toString();
                String voice = getString(R.string.plz_wait5);
                textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
            }
        }
        return new LoaderTransports(this, mStationFrom.station_id, mRoute.id);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Transport[]> loader, Transport[] data) {
        if(data != null) {
            if(data.length == 0) {
                mPlzWait.setText(R.string.empty_transport_list);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = mPlzWait.getText().toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
            } else {
                mRecyclerView.setVisibility(View.VISIBLE);
                mPlzWait.setVisibility(View.INVISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice;
                    if (mRecyclerView.getAdapter() == null) {
                        voice = getString(R.string.loaded_transport);
                        //else
                        //    voice = getString(R.string.update_transport);
                        textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                    }
                }
                if (mRecyclerView.getAdapter() == null) {
                    AdapterChoiceTransport act = new AdapterChoiceTransport(this, data);
                    mRecyclerView.setAdapter(act);
                } else {
                    ((AdapterChoiceTransport)mRecyclerView.getAdapter()).setNewItems(data);
                }
                //mRecyclerView.requestFocus();
                if(mSelectedId != null){
                    int index = -1;
                    for(int i1=0, cnt1=((AdapterChoiceTransport)mRecyclerView.getAdapter()).getItems().length; i1 < cnt1; i1++) {
                        if(((AdapterChoiceTransport)mRecyclerView.getAdapter()).getItems()[i1].gos_num.equals(mSelectedId)) {
                            index = i1;
                            break;
                        }
                    }
                    if(index != -1) {
                        final int finalIndex = index;
                        mRecyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mRecyclerView.getLayoutManager().findViewByPosition(finalIndex).requestFocus();
                                    }
                                });
                            }
                        }, 0);

                    }
                }
                mCountDownTimer = new Handler();
                mCountDownTimer.postDelayed(r, TIMEOUT_REFRESH);
            }
        } else {
            if(mRecyclerView.getAdapter() == null) {
                mRecyclerView.setVisibility(View.INVISIBLE);
                mPlzWait.setVisibility(View.VISIBLE);
                mPlzWait.setText(R.string.error_load);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    textToSpeech.speak(mPlzWait.getText().toString(), TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
            } else {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    textToSpeech.speak(getString(R.string.noupdate), TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Transport[]> loader) {

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.plz_wait) {
            if (getSupportLoaderManager().hasRunningLoaders()) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String voice = getString(R.string.plz_wait5);
                    String utteranceId = UUID.randomUUID().toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                }
            } else
                getSupportLoaderManager().restartLoader(0, null, this);
        } else
            onBackPressed();
    }
}