package com.mksmbrtsh.transportvision.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mksmbrtsh.transportvision.Loaders.LoaderRoutes;
import com.mksmbrtsh.transportvision.Models.Route;
import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.R;
import com.mksmbrtsh.transportvision.UI.AdapterChoiceRoute;
import com.mksmbrtsh.transportvision.UI.RecyclerItemClickListener;

import java.util.UUID;

public class AccessibilitySelectRouteActivity extends AccessibilityBaseActivity implements LoaderManager.LoaderCallbacks<Route[]>, View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private Button mPlzWait;
    private RecyclerView mRecyclerView;
    private TypeTransport mTypeTransport;
    private int mPosition =-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_route);
        mTypeTransport = getIntent().getParcelableExtra("type_transport");
        TextView title = (TextView)findViewById(R.id.text_choice_map_point);
        title.setText(title.getText() + " " + mTypeTransport.name);
        mRecyclerView = findViewById(R.id.list_route);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mPlzWait = findViewById(R.id.plz_wait);
        mPlzWait.setOnClickListener(this);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mPosition = position;
                    String utteranceId = UUID.randomUUID().toString();
                    Route item = ((AdapterChoiceRoute) mRecyclerView.getAdapter()).getItems()[position];
                    StringBuilder sb = new StringBuilder(item.name);
                    sb.append(": ");
                    sb.append(getString(R.string.from));
                    sb.append(' ');
                    sb.append(item.from_station_name);
                    sb.append(' ');
                    sb.append(getString(R.string.to));
                    sb.append(' ');
                    sb.append(item.to_station_name);
                    String voice = sb.toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                } else {
                    startActivitySelectStationFrom(position);
                }
            }


            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onLoadData() {
        getSupportLoaderManager().initLoader(0, null, AccessibilitySelectRouteActivity.this);
    }

    @Override
    public void onSpeakComplete() {
        if(mPosition != NOT_SELECTED)
            startActivitySelectStationFrom(mPosition);
    }

    private void startActivitySelectStationFrom(int positionRoute){
        Intent intent = new Intent(AccessibilitySelectRouteActivity.this, AccessibilitySelectStationFromActivity.class);
        intent.putExtra("route", ((AdapterChoiceRoute) mRecyclerView.getAdapter()).getItems()[positionRoute]);
        intent.putExtra("type_transport", mTypeTransport);
        this.startActivity(intent);
        Log.d(TAG, "startActivitySelectStationFrom: type_transport='" + mTypeTransport.toString() + "' route='" + ((AdapterChoiceRoute) mRecyclerView.getAdapter()).getItems()[positionRoute].toString() +"'");
        finish();
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(AccessibilitySelectRouteActivity.this, SelectTypeTransportActivity.class);
        this.startActivity(intent);
        finish();
    }


    @NonNull
    @Override
    public Loader<Route[]> onCreateLoader(int id, @Nullable Bundle args) {
        mPlzWait.setText(R.string.plz_wait2);
        mPlzWait.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setAllImportantForAccessibility(false);
            String utteranceId = UUID.randomUUID().toString();
            String voice = getString(R.string.plz_wait2);
            textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }
        return new LoaderRoutes(this, mTypeTransport.id);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Route[]> loader, Route[] data) {
        mPlzWait.setEnabled(true);
        if (data != null) {
            if(data.length == 0) {
                mPlzWait.setText(R.string.empty_route_list);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = mPlzWait.getText().toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
            } else {
                AdapterChoiceRoute actt = new AdapterChoiceRoute(data, getString(R.string.from), getString(R.string.to));
                mRecyclerView.setAdapter(actt);
                mRecyclerView.setVisibility(View.VISIBLE);
                mPlzWait.setVisibility(View.INVISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.loaded_routes);
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
                mRecyclerView.requestFocus();
            }
        } else {
            mRecyclerView.setVisibility(View.INVISIBLE);
            mPlzWait.setVisibility(View.VISIBLE);
            mPlzWait.setText(R.string.error_load);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String utteranceId = UUID.randomUUID().toString();
                textToSpeech.speak(mPlzWait.getText().toString(), TextToSpeech.QUEUE_ADD, null, utteranceId);
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Route[]> loader) {

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.plz_wait) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getSupportLoaderManager().hasRunningLoaders()) {
                String voice = getString(R.string.plz_wait2);
                String utteranceId = UUID.randomUUID().toString();
                textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
            } else
                getSupportLoaderManager().restartLoader(0, null, this);
        } else
            onBackPressed();
    }
}