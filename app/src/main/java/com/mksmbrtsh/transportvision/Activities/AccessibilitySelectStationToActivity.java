package com.mksmbrtsh.transportvision.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mksmbrtsh.transportvision.Loaders.LoaderStations;
import com.mksmbrtsh.transportvision.Models.Route;
import com.mksmbrtsh.transportvision.Models.Station;
import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.R;
import com.mksmbrtsh.transportvision.UI.AdapterChoiceStation;
import com.mksmbrtsh.transportvision.UI.RecyclerItemClickListener;

import java.util.UUID;

public class AccessibilitySelectStationToActivity extends AccessibilityBaseActivity implements LoaderManager.LoaderCallbacks<Station[]>, View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private Button mPlzWait;
    private RecyclerView mRecyclerView;

    private Route mRoute;
    private TypeTransport mTypeTransport;
    private Station mStationFrom;
    private int mPosition =-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_station_to);
        mRoute = getIntent().getParcelableExtra("route");
        mTypeTransport = getIntent().getParcelableExtra("type_transport");
        mStationFrom = getIntent().getParcelableExtra("station_from");
        TextView title = findViewById(R.id.text_choice_map_end_point);
        title.setText(title.getText() + " " + mTypeTransport.name +" " + mRoute.name + " " +getString(R.string.from) + " " + mStationFrom.station_name);
        mRecyclerView = findViewById(R.id.list_station_end);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mPlzWait = findViewById(R.id.plz_wait);
        mPlzWait.setOnClickListener(this);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mPosition = position;
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.choice) + " " + ((AdapterChoiceStation) mRecyclerView.getAdapter()).getItems()[position].station_name;
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                } else {
                    startActivitySelectTransport(position);
                }

            }


            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onLoadData() {
        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onSpeakComplete() {
        if(mPosition != NOT_SELECTED)
            startActivitySelectTransport(mPosition);
    }

    private void startActivitySelectTransport(int positionStationTo) {
        Intent intent = new Intent(AccessibilitySelectStationToActivity.this, AccessibilitySelectTransportActivity.class);
        intent.putExtra("station_from",mStationFrom);
        intent.putExtra("route", mRoute);
        intent.putExtra("type_transport", mTypeTransport);
        intent.putExtra("station_to",((AdapterChoiceStation) mRecyclerView.getAdapter()).getItems()[positionStationTo]);
        Log.d(TAG, "startActivitySelectTransport: type_transport='" + mTypeTransport.toString() + "' route='" + mRoute.toString() +"' station_from='" + mStationFrom.toString() +"' station_to='" +((AdapterChoiceStation) mRecyclerView.getAdapter()).getItems()[positionStationTo].toString() + "'");
        this.startActivity(intent);
        finish();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AccessibilitySelectStationToActivity.this, AccessibilitySelectStationFromActivity.class);
        intent.putExtra("station_from",mStationFrom);
        intent.putExtra("route", mRoute);
        intent.putExtra("type_transport", mTypeTransport);
        this.startActivity(intent);
        finish();
    }

    @NonNull
    @Override
    public Loader<Station[]> onCreateLoader(int id, @Nullable Bundle args) {
        mPlzWait.setText(R.string.plz_wait4);
        mPlzWait.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setAllImportantForAccessibility(false);
            String utteranceId = UUID.randomUUID().toString();
            String voice = getString(R.string.plz_wait4);
            textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }
        return new LoaderStations(this, mRoute.id, mStationFrom.station_id, mStationFrom.ord);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Station[]> loader, Station[] data) {
        if(data != null) {
            if(data.length == 0){
                mPlzWait.setText(R.string.empty_station_to_list);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = mPlzWait.getText().toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
            } else {
                AdapterChoiceStation actt = new AdapterChoiceStation(data);
                mRecyclerView.setAdapter(actt);
                mRecyclerView.setVisibility(View.VISIBLE);
                mPlzWait.setVisibility(View.INVISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String utteranceId = UUID.randomUUID().toString();
                    String voice = getString(R.string.loaded_station_to);
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_ADD, null, utteranceId);
                }
                mRecyclerView.requestFocus();
            }
        } else {
            mRecyclerView.setVisibility(View.INVISIBLE);
            mPlzWait.setVisibility(View.VISIBLE);
            mPlzWait.setText(R.string.error_load);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String utteranceId = UUID.randomUUID().toString();
                textToSpeech.speak(mPlzWait.getText().toString(), TextToSpeech.QUEUE_ADD, null, utteranceId);
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Station[]> loader) {

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.plz_wait) {
            if (getSupportLoaderManager().hasRunningLoaders()) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String voice = getString(R.string.plz_wait4);
                    String utteranceId = UUID.randomUUID().toString();
                    textToSpeech.speak(voice, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                }
            } else
                getSupportLoaderManager().restartLoader(0, null, this);
        } else
            onBackPressed();
    }
}