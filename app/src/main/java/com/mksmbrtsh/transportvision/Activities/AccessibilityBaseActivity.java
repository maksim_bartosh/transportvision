package com.mksmbrtsh.transportvision.Activities;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class AccessibilityBaseActivity extends AppCompatActivity {

    private ArrayList<View> mAllViews = new ArrayList<View>();
    protected TextToSpeech textToSpeech;
    protected int NOT_SELECTED = -1;

    public abstract void onLoadData();
    public abstract void onSpeakComplete();

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        mAllViews = getAllChildren(view);
        prepareLoad();
    }
    @SuppressLint("ResourceType")
    @Override
    public void setContentView(int id) {
        super.setContentView(id);
        mAllViews = getAllChildren(getWindow().getDecorView().getRootView());
        prepareLoad();
    }

    @Override
    public void onDestroy() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (textToSpeech != null) {
                textToSpeech.stop();
                textToSpeech.shutdown();
            }
        }
        super.onDestroy();
    }
    private void prepareLoad() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setAllImportantForAccessibility(false);
            textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    textToSpeech.setOnUtteranceProgressListener(mUtteranceProgressListener);
                    mAllViews.get(0).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onLoadData();
                                }
                            });
                        }
                    },2000);

                }
            });
        }
        else
            onLoadData();
    }



    protected void setAllImportantForAccessibility(boolean ok) {
        for(View v : mAllViews) {
            if(v instanceof RecyclerView){
                v.setImportantForAccessibility(ok? View.IMPORTANT_FOR_ACCESSIBILITY_YES: View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS);
            } else
                v.setImportantForAccessibility(ok? View.IMPORTANT_FOR_ACCESSIBILITY_YES: View.IMPORTANT_FOR_ACCESSIBILITY_NO);
        }
    }

    private ArrayList<View> getAllChildren(View v) {

        if (v instanceof RecyclerView || v instanceof TextView || v instanceof Button || v instanceof ImageView) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            return viewArrayList;
        }
        ArrayList<View> result = new ArrayList<View>();
        if(v instanceof ConstraintLayout) {
            result.add(v);
        }
        if ((v instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) v;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                result.addAll(getAllChildren(child));
            }
        }
        return result;
    }

    private UtteranceProgressListener mUtteranceProgressListener = new UtteranceProgressListener() {

        @Override
        public void onStart(String s) {

        }

        @Override
        public void onDone(String s) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setAllImportantForAccessibility(true);
                    onSpeakComplete();
                }
            });
        }

        @Override
        public void onError(String s) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setAllImportantForAccessibility(true);
                }
            });
        }

    };

}
