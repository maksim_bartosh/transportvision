package com.mksmbrtsh.transportvision.utils;

import android.content.Context;

import com.mksmbrtsh.transportvision.R;

public class TimeString {
    public static String SecondToTimeStr(Context context, int totalSecond) {
        int sec = totalSecond % 60;
        int min = (totalSecond / 60)%60;
        int hours = (totalSecond / 60)/60;
        StringBuilder sb = new StringBuilder();
        if(totalSecond <= 59) {
            sb.append(' ');
            sb.append(context.getString(R.string.now));
            return sb.toString();
        }
        sb.append(' ');
        sb.append(context.getString(R.string.across));
        sb.append(' ');
        if(hours>0) {
            sb.append(hours);
            sb.append(' ');
            sb.append(getAddition(hours, context.getResources().getStringArray(R.array.hours)));
            sb.append(", ");
        }
        sb.append(min);
        sb.append(' ');
        sb.append(getAddition(min, context.getResources().getStringArray(R.array.minutes)));
        return sb.toString();
    }

    public static String getAddition(int num, String[] s) {

        int n0 = num % 100;
        int n1 = n0 / 10;

        if(n0  > 10 && n0 < 20)
            return s[2];

        switch (num % 10) {
            case 1:
                return s[0];//минуту
            case 2:
            case 3:
            case 4:
                return s[1];//минуты
            default:
                return s[2];// минут
        }

    }

}
