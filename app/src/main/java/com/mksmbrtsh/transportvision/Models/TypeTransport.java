package com.mksmbrtsh.transportvision.Models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class TypeTransport implements Parcelable {
    public String id;
    public String name;
    public String type;

    public TypeTransport(String id, String name, String type){
        this.id = id;
        this. name = name;
        this.type = type;
    }

    protected TypeTransport(Parcel in) {
        id = in.readString();
        name = in.readString();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TypeTransport> CREATOR = new Creator<TypeTransport>() {
        @Override
        public TypeTransport createFromParcel(Parcel in) {
            return new TypeTransport(in);
        }

        @Override
        public TypeTransport[] newArray(int size) {
            return new TypeTransport[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return "id='" + id + "' name= '" + name +"' type='" + type +"'";
    }
}
