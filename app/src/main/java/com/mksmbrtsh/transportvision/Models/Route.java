package com.mksmbrtsh.transportvision.Models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Route  implements Parcelable {
    public String id;
    public String name;
    public String number;
    public String is_subroute;
    public String from_station_id;
    public String to_station_id;
    public String is_cycle;
    public String from_station_name;
    public String to_station_name;

    public Route(String id,
             String name,
             String number,
             String is_subroute,
             String from_station_id,
             String to_station_id,
             String is_cycle,
             String from_station_name,
             String to_station_name){
        this.id = id;
        this.name = name;
        this.number = number;
        this.is_subroute = is_subroute;
        this.from_station_id = from_station_id;
        this.to_station_id = to_station_id;
        this.is_cycle = is_cycle;
        this.from_station_name = from_station_name;
        this.to_station_name = to_station_name;
    }

    protected Route(Parcel in) {
        id = in.readString();
        name = in.readString();
        number = in.readString();
        is_subroute = in.readString();
        from_station_id = in.readString();
        to_station_id = in.readString();
        is_cycle = in.readString();
        from_station_name = in.readString();
        to_station_name = in.readString();
    }

    public static final Creator<Route> CREATOR = new Creator<Route>() {
        @Override
        public Route createFromParcel(Parcel in) {
            return new Route(in);
        }

        @Override
        public Route[] newArray(int size) {
            return new Route[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(number);
        parcel.writeString(is_subroute);
        parcel.writeString(from_station_id);
        parcel.writeString(to_station_id);
        parcel.writeString(is_cycle);
        parcel.writeString(from_station_name);
        parcel.writeString(to_station_name);
    }

    @NonNull
    @Override
    public String toString() {
        return "id='" + id + "' name='" + name + "' number='" + number + "' is_subroute='" + is_subroute + "' from_station_id='" + from_station_id + "' to_station_id='" + to_station_id + "' is_cycle='" + is_cycle + "' from_station_name='" + from_station_name + "' to_station_name='" + to_station_name + "'";
    }
}
