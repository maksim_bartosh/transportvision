package com.mksmbrtsh.transportvision.Models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Vehicle implements Parcelable {
    public String stid;
    public String stname;
    private Integer arrt;

    public Vehicle(String stid, String stname, Integer arrt){
        this.stid = stid;
        this.stname = stname;
        this.arrt = arrt;
    }

    public boolean setArrt(Integer newArrt){
        int newArrtMin = (newArrt / 60)%60;
        int newArrtHours = (newArrt / 60)/60;
        int min = (arrt / 60)%60;
        int hours = (arrt / 60)/60;
        arrt = newArrt;
        if(min > newArrtMin || hours > newArrtHours)
            return true;
        else return false;
    }

    public int getArrt() {
        return arrt;
    }

    protected Vehicle(Parcel in) {
        stid = in.readString();
        stname = in.readString();
        if (in.readByte() == 0) {
            arrt = null;
        } else {
            arrt = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stid);
        dest.writeString(stname);
        if (arrt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(arrt);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Vehicle> CREATOR = new Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return "stid='" + stid + "' stname='"+stname+"' arrt='" + arrt + "'";
    }
}
