package com.mksmbrtsh.transportvision.Models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Station implements Parcelable {
    public Integer station_id;
    public Integer ord;
    public String station_name;
    public String station_description;

    public Station(Integer station_id,
            Integer ord,
            String station_name,
            String station_description) {
        this.station_id = station_id;
        this.ord =ord;
        this.station_name = station_name;
        this.station_description = station_description;
    }

    protected Station(Parcel in) {
        if (in.readByte() == 0) {
            station_id = null;
        } else {
            station_id = in.readInt();
        }
        if (in.readByte() == 0) {
            ord = null;
        } else {
            ord = in.readInt();
        }
        station_name = in.readString();
        station_description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (station_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(station_id);
        }
        if (ord == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ord);
        }
        dest.writeString(station_name);
        dest.writeString(station_description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Station> CREATOR = new Creator<Station>() {
        @Override
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        @Override
        public Station[] newArray(int size) {
            return new Station[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return "station_id='" + station_id + "' ord='" + ord + "' station_name ='" + station_name + "' station_description='" + station_description + "'";
    }
}
