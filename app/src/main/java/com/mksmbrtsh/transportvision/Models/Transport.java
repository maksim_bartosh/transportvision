package com.mksmbrtsh.transportvision.Models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Transport implements Parcelable {
    public String rid;
    public String rnum;
    public String rtype;
    public String obj_id;
    public Integer arrt;
    public String last;
    public String where;
    public String obj_tags;
    public String gos_num;

    public Transport(String rid,
            String rnum,
            String rtype,
            String obj_id,
            Integer arrt,
            String last,
            String where,
            String obj_tags,
            String gos_num) {
        this.rid= rid;
        this.rnum= rnum;
        this.rtype= rtype;
        this.obj_id=obj_id;
        this.arrt = arrt;
        this.last=last;
        this.where = where;
        this.obj_tags = obj_tags;
        this.gos_num = gos_num;
    }

    protected Transport(Parcel in) {
        rid = in.readString();
        rnum = in.readString();
        rtype = in.readString();
        obj_id = in.readString();
        if (in.readByte() == 0) {
            arrt = null;
        } else {
            arrt = in.readInt();
        }
        last = in.readString();
        where = in.readString();
        obj_tags = in.readString();
        gos_num = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rid);
        dest.writeString(rnum);
        dest.writeString(rtype);
        dest.writeString(obj_id);
        if (arrt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(arrt);
        }
        dest.writeString(last);
        dest.writeString(where);
        dest.writeString(obj_tags);
        dest.writeString(gos_num);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Transport> CREATOR = new Creator<Transport>() {
        @Override
        public Transport createFromParcel(Parcel in) {
            return new Transport(in);
        }

        @Override
        public Transport[] newArray(int size) {
            return new Transport[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return "rid='" + rid + "' rnum='" + rnum + "' rtype='" + rtype + "obj_id='" + obj_id + "' arrt='" + arrt + "' last='" + last + "' where='" + where + "obj_tags='" + obj_tags + "gos_num='" + gos_num + "'";
    }
}
