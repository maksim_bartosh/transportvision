package com.mksmbrtsh.transportvision.Loaders;

import android.content.Context;
import android.text.NoCopySpan;
import android.util.Log;

import com.mksmbrtsh.transportvision.Models.TypeTransport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.mksmbrtsh.transportvision.Const.HOST;
import static com.mksmbrtsh.transportvision.Endpoints.TYPES_TRANSPORT;

public class LoaderTypesTransport extends AsyncTaskLoader<TypeTransport[]> {
    private final String TAG = this.getClass().getSimpleName();

    public LoaderTypesTransport(@NonNull Context context) {
        super(context);

    }
    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Nullable
    @Override
    public TypeTransport[] loadInBackground() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();
        Request request = new Request.Builder().url(HOST + TYPES_TRANSPORT).build();
        try {
            Response response = client.newCall(request).execute();
            String resStr = response.body().string();
            JSONArray js = new JSONArray(resStr);
            TypeTransport[] typesTransport = new TypeTransport[js.length()];
            for(int i1 = 0, cnt1 = js.length(); i1<cnt1;i1++) {
                JSONObject j = js.getJSONObject(i1);
                TypeTransport t = new TypeTransport(j.getString("id"), j.getString("name"), j.getString("type"));
                typesTransport[i1] = t;
            }
            return typesTransport;
        } catch (IOException | JSONException e) {
            Log.d(TAG,"load type transport fail", e);
            return null;
        }
    }
}
