package com.mksmbrtsh.transportvision.Loaders;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import com.mksmbrtsh.transportvision.Models.Route;
import com.mksmbrtsh.transportvision.Models.TypeTransport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.mksmbrtsh.transportvision.Const.HOST;
import static com.mksmbrtsh.transportvision.Endpoints.ROUTES;
import static com.mksmbrtsh.transportvision.Endpoints.TYPES_TRANSPORT;

public class LoaderRoutes extends AsyncTaskLoader<Route[]> {
    private final String TAG = this.getClass().getSimpleName();
    private final String tid;

    public LoaderRoutes(@NonNull Context context, String tid) {
        super(context);
        this.tid = tid;
    }
    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Nullable
    @Override
    public Route[] loadInBackground() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();
        Request request = new Request.Builder().url(HOST + ROUTES + "&tid=" + tid).build();
        try {
            Response response = client.newCall(request).execute();
            String resStr = response.body().string();
            JSONArray js = new JSONArray(resStr);
            Route[] routes = new Route[js.length()];
            for(int i1 = 0, cnt1 = js.length(); i1<cnt1;i1++) {
                JSONObject j = js.getJSONObject(i1);
                Route t = new Route(
                        j.getString("id"),
                        j.getString("name"),
                        j.getString("number"),
                        j.getString("is_subroute"),
                        j.getString("from_station_id"),
                        j.getString("to_station_id"),
                        j.getString("is_cycle"),
                        j.getString("from_station_name"),
                        j.getString("to_station_name")
                );
                routes[i1] = t;
            }
            return routes;
        } catch (IOException | JSONException e) {
            Log.d(TAG,"load route fail", e);
            return null;
        }
    }
}
