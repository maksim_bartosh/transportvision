package com.mksmbrtsh.transportvision.Loaders;

import android.content.Context;
import android.util.Log;

import com.mksmbrtsh.transportvision.Models.Station;
import com.mksmbrtsh.transportvision.Models.Vehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.mksmbrtsh.transportvision.Const.HOST;
import static com.mksmbrtsh.transportvision.Endpoints.VEHICLE_FORECAST_WITH_NAMES;

public class LoaderVehicle extends AsyncTaskLoader<LoaderVehicle.RefreshListVehicles> {
    private final String TAG = this.getClass().getSimpleName();

    public enum STATUS {
        OLD_CURRENT,
        REFRESH_CURRENT,
        IO_ERROR,
        JSON_ERROR,
        ARRAY_JSON_EMPTY,
        ALL_STATION_DEPRECATED,
        NEW_CURRENT_STATION,
    }
    public class RefreshListVehicles {
        public ArrayList<Vehicle> deprecatedVehicles;
        public STATUS status;
        public Vehicle current;
        public ArrayList<Vehicle> allVehicles;

        public RefreshListVehicles(ArrayList<Vehicle> deprecatedVehicles, Vehicle current, STATUS status, ArrayList<Vehicle> allVehicles) {
            this.deprecatedVehicles = deprecatedVehicles;
            this.current = current;
            this.status = status;
            this.allVehicles = allVehicles;
        }
    }

    private final ArrayList<Vehicle> mDeprecatedVehicles;
    private String mVehid;
    private Vehicle mCurrentStation;
    private Station mStationFrom;
    public ArrayList<Vehicle> mAllVehicles;

    public LoaderVehicle(@NonNull Context context, String vehid, ArrayList<Vehicle> deprecatedVehicles, Vehicle currentStation, Station stationFrom, ArrayList<Vehicle> allVehicles) {
        super(context);
        mVehid = vehid;
        mDeprecatedVehicles = deprecatedVehicles;
        mCurrentStation = currentStation;
        mStationFrom = stationFrom;
        mAllVehicles = allVehicles;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Nullable
    @Override
    public LoaderVehicle.RefreshListVehicles loadInBackground() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();
        Request request = new Request.Builder().url(HOST + VEHICLE_FORECAST_WITH_NAMES + "&vehid=" + mVehid).build();
        try {
            Response response = client.newCall(request).execute();
            String resStr = response.body().string();
            JSONArray js = new JSONArray(resStr);
            if(js.length() == 0) {
                Log.d(TAG,"load empty json");
                RefreshListVehicles r= new RefreshListVehicles(mDeprecatedVehicles, mCurrentStation, STATUS.ARRAY_JSON_EMPTY, null);
                return r;
            }
            ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
            // взять все, кроме устаревших
            for(int i1 = 0, cnt1 = js.length(); i1 < cnt1; i1++) {
                JSONObject j = js.getJSONObject(i1);
                String stid = j.getString("stid");
                for(int i2= 0,cnt2 = mDeprecatedVehicles.size(); i2 < cnt2;i2++) {
                    if(mDeprecatedVehicles.get(i2).stid.equals(stid)) {
                        Log.d(TAG,"miss deprecated route point: " + stid);
                        continue;
                    }
                }
                Vehicle v = new Vehicle(stid, j.getString("stname"), Integer.valueOf(j.getString("arrt")));
                vehicles.add(v);
            }
            if(vehicles.size() == 0) {
                Log.d(TAG,"all station deprecated");
                RefreshListVehicles r= new RefreshListVehicles(mDeprecatedVehicles, mCurrentStation, STATUS.ALL_STATION_DEPRECATED, null);
                return r;
            }
            // найти текущую
            int index = -1;
            if(mCurrentStation != null) {
                for (int i1 = 0, cnt1 = vehicles.size(); i1 < cnt1; i1++) {
                    if (mCurrentStation.stid.equals(vehicles.get(i1).stid)) {
                        index = i1;
                        Log.d(TAG,"current station '" +mCurrentStation.stid + " " + mCurrentStation.stname +"' found in json");
                        break;
                    }
                }
            }
            Comparator<Vehicle> comporatorVehicles = new Comparator<Vehicle>() {
                @Override
                public int compare(Vehicle vehicle1, Vehicle vehicle2) {
                    return vehicle1.getArrt() > vehicle2.getArrt() ? 1 : -1;
                }
            };
            // выстроить по возрастанию времени ожидания все непройденные остановки
            Collections.sort(vehicles, comporatorVehicles);
            if(index != -1) {
                // текущая найдена, определить изменилось ли время более чем на 1 минуту
                STATUS s = mCurrentStation.setArrt(vehicles.get(index).getArrt())? STATUS.REFRESH_CURRENT : STATUS.OLD_CURRENT;
                Log.d(TAG,"status: " + s.toString());
                Log.d(TAG,"deprecated vehicles: " + mDeprecatedVehicles.toString());
                RefreshListVehicles r= new RefreshListVehicles(mDeprecatedVehicles, mCurrentStation, s, vehicles);
                return r;
            }
            // текущая не найдена
            if(mCurrentStation != null)
                Log.d(TAG,"current station '" +mCurrentStation.stid + " " + mCurrentStation.stname +"' NOT FOUND in json");
            else
                Log.d(TAG,"current station is null");


            // найти начальную остановку
            String n = mStationFrom.station_id.toString();
            for (int i1 = 0, cnt1 = vehicles.size(); i1 < cnt1; i1++) {
                if (n.equals(vehicles.get(i1).stid)) {
                    index = i1;
                    Log.d(TAG,"station from '" + mStationFrom.station_id + " " + mStationFrom.station_name +"' found in json");
                    break;
                }
            }
            if(index != -1) {
                // начальную остановку ещё не проехали, сделать её текущей, для расчёта времени до неё
                STATUS s;
                if(mCurrentStation == null) {
                    s = STATUS.NEW_CURRENT_STATION;
                    mCurrentStation = vehicles.get(index);
                    Log.d(TAG,"current station set to station from");
                } else {
                    s = mCurrentStation.setArrt(vehicles.get(index).getArrt())? STATUS.REFRESH_CURRENT : STATUS.OLD_CURRENT;
                    Log.d(TAG,"current station = station from update");
                }
                Log.d(TAG,"status: " + s);
                RefreshListVehicles r = new RefreshListVehicles(mDeprecatedVehicles, mCurrentStation, s, vehicles);
                return r;
            } else {
                // нет начальной остановки, её проехали
                boolean contain_ok = false;
                Log.d(TAG,"station from NOT FOUND in json");
                if(mCurrentStation != null) {
                    Log.d(TAG,"current station is deprecated");
                    // и текущю проехали, добавить её в список пройденных
                    for(Vehicle v : mDeprecatedVehicles)
                    {
                        if(v.stid.equals(mCurrentStation.stid)) {
                            contain_ok = true;
                            Log.d(TAG,"current station NOT FOUND in deprecated list");
                            break;
                        }
                    }
                    if(!contain_ok) {
                        Log.d(TAG,"deprecated vehicles add : " + mCurrentStation.toString());
                        mDeprecatedVehicles.add(mCurrentStation);
                    }
                }
                // означить новую текущую остановку
                mCurrentStation = vehicles.get(0);
                Log.d(TAG,"set current station: " + mCurrentStation.toString());
            }
            Log.d(TAG,"status: " + STATUS.NEW_CURRENT_STATION);
            RefreshListVehicles r= new RefreshListVehicles(mDeprecatedVehicles, mCurrentStation, STATUS.NEW_CURRENT_STATION, vehicles);
            return r;
        } catch (IOException e) {
            Log.d(TAG,"status: " + STATUS.IO_ERROR);
            Log.d(TAG,"load vehicles fail", e);
            RefreshListVehicles r= new RefreshListVehicles(mDeprecatedVehicles, mCurrentStation, STATUS.IO_ERROR, null);
            return r;
        } catch (JSONException e) {
            Log.d(TAG,"status: " + STATUS.JSON_ERROR);
            Log.d(TAG,"load vehicles fail", e);
            RefreshListVehicles r= new RefreshListVehicles(mDeprecatedVehicles, mCurrentStation, STATUS.JSON_ERROR, null);
            return r;
        }
    }
}
