package com.mksmbrtsh.transportvision.Loaders;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import com.mksmbrtsh.transportvision.Models.Station;
import com.mksmbrtsh.transportvision.Models.Transport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import static com.mksmbrtsh.transportvision.Const.HOST;
import static com.mksmbrtsh.transportvision.Endpoints.STATION_FORECAST_WITH_TAGS;

public class LoaderTransports extends AsyncTaskLoader<Transport[]> {
    private final String TAG = this.getClass().getSimpleName();
    private int mIdStationFrom;
    private String mIdRoute;

    public LoaderTransports(@NonNull Context context, int idStationFrom, String idRoute) {
        super(context);
        mIdStationFrom = idStationFrom;
        this.mIdRoute = idRoute;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Nullable
    @Override
    public Transport[] loadInBackground() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();
        Request request = new Request.Builder().url(HOST + STATION_FORECAST_WITH_TAGS + "&sid=" + mIdStationFrom).build();
        try {
            Response response = client.newCall(request).execute();
            String resStr = response.body().string();
            JSONArray js = new JSONArray(resStr);
            ArrayList<Transport> transport= new  ArrayList<Transport>();
            for(int i1 = 0, cnt1 = js.length(); i1<cnt1;i1++) {
                JSONObject j = js.getJSONObject(i1);
                int i = 0;
                if(j.isNull("arrt")) {
                    Log.d(TAG,"miss arrt for: " + j.toString());
                } else {
                    i = Integer.valueOf(j.getString("arrt"));
                }
                if(j.getString("rid").equals(mIdRoute)) {
                    Transport t = new Transport(
                            j.getString("rid"),
                            j.getString("rnum"),
                            j.getString("rtype"),
                            j.getString("obj_id"),
                            i,
                            j.getString("last"),
                            j.getString("where"),
                            null,
                            j.getString("gos_num"));
                    transport.add(t);
                }
            }
            Comparator<Transport> v = new Comparator<Transport>() {
                @Override
                public int compare(Transport transport1, Transport transport2) {
                    if(transport1.arrt > transport2.arrt)
                        return 1;
                    else
                        return -1;
                }
            };
            Transport[] s = new Transport[transport.size()];
            s = transport.toArray(s);
            Arrays.sort(s,v);
            Log.d(TAG,"final sort list: " + Arrays.toString(s));
            return s;
        } catch (IOException | JSONException e) {
            Log.d(TAG,"load transport fail", e);
            return null;
        }
    }
}
