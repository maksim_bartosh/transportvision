package com.mksmbrtsh.transportvision.Loaders;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import com.mksmbrtsh.transportvision.Models.Station;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import static com.mksmbrtsh.transportvision.Const.HOST;
import static com.mksmbrtsh.transportvision.Endpoints.STATION_FROM;

public class LoaderStations extends AsyncTaskLoader<Station[]> {
    private final String TAG = this.getClass().getSimpleName();
    private final String mIdRoute;
    private final int mIdStationFrom;
    private final int mOrdStationFrom;

    public LoaderStations(@NonNull Context context, String mIdRoute, int mIdStationFrom, int mOrdStationFrom) {
        super(context);
        this.mIdRoute = mIdRoute;
        this.mIdStationFrom = mIdStationFrom;
        this.mOrdStationFrom = mOrdStationFrom;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Nullable
    @Override
    public Station[] loadInBackground() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();
        Request request = new Request.Builder().url(HOST + STATION_FROM + "&id=" + mIdRoute).build();
        if(mIdStationFrom != -1)
            Log.d(TAG,"load station to...");
        else
            Log.d(TAG,"load station from...");
        try {
            Response response = client.newCall(request).execute();
            String resStr = response.body().string();
            JSONArray js = new JSONArray(resStr);
            ArrayList<Station> station = new ArrayList<Station>(js.length());
            for(int i1 = 0, cnt1 = js.length(); i1<cnt1;i1++) {
                JSONObject j = js.getJSONObject(i1);
                Station t = new Station(j.getInt("station_id"),
                        j.getInt("ord"),
                        j.getString("station_name"),
                        j.getString("station_description")
                );
                if(t.ord > mOrdStationFrom)
                    station.add(t);
            }
            Comparator<Station> v = new Comparator<Station>() {
                @Override
                public int compare(Station station1, Station station2) {
                    if(station1.ord > station2.ord)
                        return 1;
                    else
                        return -1;
                }
            };
            Station[] s = new Station[station.size()];
            s = station.toArray(s);
            Arrays.sort(s,v);
            Log.d(TAG,"final sort list: " + Arrays.toString(s));
            return s;
        } catch (IOException | JSONException e) {
            if(mIdStationFrom != -1)
                Log.d(TAG,"load station to fail", e);
            else
                Log.d(TAG,"load station from fail", e);
            return null;
        }
    }
}
