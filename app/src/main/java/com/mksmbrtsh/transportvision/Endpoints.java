package com.mksmbrtsh.transportvision;

import static com.mksmbrtsh.transportvision.Const.CITY;

public class Endpoints {
    public static String TYPES_TRANSPORT = "/getRouteTypes.php?city=" + CITY;
    public static String ROUTES = "/getAllRoutes.php?city=" + CITY;
    public static String STATION_FROM = "/getRouteStations.php?city=" + CITY;
    public static String STATION_FORECAST_WITH_TAGS = "/getStationForecastsWithTags.php?city=" + CITY;
    public static String VEHICLE_FORECAST_WITH_NAMES = "/getVehicleForecastsWithNames.php?city=" + CITY;
}
