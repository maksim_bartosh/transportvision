package com.mksmbrtsh.transportvision.UI;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mksmbrtsh.transportvision.Models.Route;
import com.mksmbrtsh.transportvision.Models.Station;
import com.mksmbrtsh.transportvision.R;

public class AdapterChoiceStation extends RecyclerView.Adapter<AdapterChoiceStation.RouteViewHolder>{

    private Station[] mDataset;

    public AdapterChoiceStation(Station[] myDataset) {
        mDataset = myDataset;
    }

    @NonNull
    @Override
    public RouteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_type_transport, parent, false);
        RouteViewHolder typeTransportViewHolder = new RouteViewHolder(v);
        return typeTransportViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RouteViewHolder holder, int position) {
        String s;
        if(mDataset[position].station_description.equals("") || mDataset[position].station_description.equals(" "))
            holder.name.setText(mDataset[position].station_name);
        else
            holder.name.setText(mDataset[position].station_name +"\n" + mDataset[position].station_description);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public class RouteViewHolder extends RecyclerView.ViewHolder  {
        public TextView name;
        public RouteViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
        }
    }

    public Station[] getItems(){
        return mDataset;
    }
}
