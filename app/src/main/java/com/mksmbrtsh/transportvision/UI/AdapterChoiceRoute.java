package com.mksmbrtsh.transportvision.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mksmbrtsh.transportvision.Models.Route;
import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.R;

public class AdapterChoiceRoute extends RecyclerView.Adapter<AdapterChoiceRoute.RouteViewHolder>{

    private Route[] mDataset;

    private String mFrom;
    private String mTo;

    public AdapterChoiceRoute(Route[] myDataset, String from, String to) {
        mDataset = myDataset;
        mFrom = from;
        mTo = to;
    }

    @NonNull
    @Override
    public RouteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_type_transport, parent, false);
        RouteViewHolder typeTransportViewHolder = new RouteViewHolder(v);
        return typeTransportViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RouteViewHolder holder, int position) {
        StringBuilder sb = new StringBuilder(mDataset[position].name);
        sb.append(": ");
        sb.append(mFrom);
        sb.append(' ');
        sb.append(mDataset[position].from_station_name);
        sb.append(' ');
        sb.append(mTo);
        sb.append(' ');
        sb.append(mDataset[position].to_station_name);
        holder.name.setText(sb.toString());
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public class RouteViewHolder extends RecyclerView.ViewHolder  {
        public TextView name;
        public RouteViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
        }
    }

    public Route[] getItems(){
        return mDataset;
    }
}
