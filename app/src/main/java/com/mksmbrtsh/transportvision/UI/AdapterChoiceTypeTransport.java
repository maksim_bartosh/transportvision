package com.mksmbrtsh.transportvision.UI;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterChoiceTypeTransport extends RecyclerView.Adapter<AdapterChoiceTypeTransport.TypeTransportViewHolder>{

    private TypeTransport[] mDataset;

    public AdapterChoiceTypeTransport(TypeTransport[] myDataset) {
        mDataset = myDataset;
    }

    @NonNull
    @Override
    public TypeTransportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_type_transport, parent, false);
        TypeTransportViewHolder typeTransportViewHolder = new TypeTransportViewHolder(v);
        return typeTransportViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TypeTransportViewHolder holder, int position) {
        holder.name.setText(mDataset[position].name);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public class TypeTransportViewHolder extends RecyclerView.ViewHolder  {
        public TextView name;
        public TypeTransportViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
        }
    }

    public TypeTransport[] getItems(){
        return mDataset;
    }
}
