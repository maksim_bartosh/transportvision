package com.mksmbrtsh.transportvision.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mksmbrtsh.transportvision.Models.Transport;
import com.mksmbrtsh.transportvision.Models.TypeTransport;
import com.mksmbrtsh.transportvision.R;
import com.mksmbrtsh.transportvision.utils.TimeString;

public class AdapterChoiceTransport extends RecyclerView.Adapter<AdapterChoiceTransport.TypeTransportViewHolder> {

    private Transport[] mDataset;
    private Context mContext;

    public AdapterChoiceTransport(Context context, Transport[] myDataset) {
        mDataset = myDataset;
        mContext = context;
    }

    @NonNull
    @Override
    public TypeTransportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_type_transport, parent, false);
        TypeTransportViewHolder typeTransportViewHolder = new TypeTransportViewHolder(v);
        return typeTransportViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeTransportViewHolder holder, int position) {
        holder.name.setText(getArrive(position));
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public String getArrive(int position) {
        StringBuilder sb = new StringBuilder(mContext.getText(R.string.gosnum));
        sb.append(' ');
        sb.append(mDataset[position].gos_num);
        sb.append(' ');
        sb.append(mContext.getString(R.string.arrives));
        sb.append(' ');
        sb.append(TimeString.SecondToTimeStr(mContext, mDataset[position].arrt));
        return sb.toString();
    }

    public void setNewItems(Transport[] data) {
        mDataset = data;
        this.notifyDataSetChanged();
    }

    public class TypeTransportViewHolder extends RecyclerView.ViewHolder  {
        public TextView name;
        public TypeTransportViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.name);
        }
    }

    public Transport[] getItems(){
        return mDataset;
    }
}
